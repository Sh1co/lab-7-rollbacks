# Lab7 - Rollbacks and reliability

## Part 1

Move all the requests under the same `with` statement, so that if any of them fail psycopg2 can rollback.

## Part 2

Create inventory table:

```sql
CREATE TABLE Inventory (
    username VARCHAR(50) NOT NULL,
    product VARCHAR(50) NOT NULL,
    amount INTEGER NOT NULL CHECK (amount >= 0 AND amount <= 100),
    PRIMARY KEY (username, product_name)
);
```

Since the product amount is checked in the database, we'll just have to check for CheckViolation in our code and handle it like how we did in the other tables.
