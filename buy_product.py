import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = (
    "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
)
add_product_to_inventory = "UPDATE Inventory SET amount = amount + %(amount)s WHERE username =  %(username)s AND product = %(product)s"


def get_connection():
    return psycopg2.connect(
        dbname="lab7",
        user="sh1co",
        password="ijbGBF78BijF7bF",
        host="127.0.0.1",
        port=5333,
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                conn.rollback()
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)

                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                conn.rollback()
                raise Exception("Product is out of stock")

            try:
                cur.execute(add_product_to_inventory, obj)

                if cur.rowcount != 1:
                    raise Exception("Wrong username or product name")
            except psycopg2.errors.CheckViolation as e:
                conn.rollback()
                raise Exception("Inventory stock out of bounds")
